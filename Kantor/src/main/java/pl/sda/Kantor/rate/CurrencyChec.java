package pl.sda.Kantor.rate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CurrencyChec {

    @Id
    @GeneratedValue
    private Integer id;

    private long timestamp;
    private String currency;
    private Double ammont;

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setAmmont(Double ammont) {
        this.ammont = ammont;
    }
}
