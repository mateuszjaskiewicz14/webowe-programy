package com.sda.dropbox.mail.client;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.mail.MailSender;
import com.sendgrid.*;
import java.io.IOException;

import static com.sda.dropbox.config.Keys.MAIL_CLIENT;
import static com.sda.dropbox.config.Keys.MAIL_KEY_GREAD;

public class MailGrid implements MailSender {
   private final ConfigService cfg;

    public MailGrid(ConfigService cfg) {
        this.cfg = cfg;
    }

    @Override
    public void seandMail(com.sda.dropbox.mail.Email e) {

            Email from = new Email(MAIL_CLIENT);
            Email to = new Email(e.getTo());
            Content content = new Content("text/plain", e.getContent());
            Mail mail = new Mail(from, e.getSubject(), to, content);

            SendGrid sg = new SendGrid(System.getenv(MAIL_KEY_GREAD));
            Request request = new Request();
            try {
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                Response response = sg.api(request);
               } catch (IOException ex) {
                throw new SecurityException("Could not email exception" + e.getTo(),ex);
            }

    }
}
