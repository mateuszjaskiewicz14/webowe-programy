package pl.sda.Kantor.contact;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.util.calendar.BaseCalendar;
import sun.util.calendar.LocalGregorianCalendar;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Controller
public class ContactController {
     @GetMapping("/contact")
    public String contact(Model model){
         SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
         Date now = new Date();
         String strDate = sdfDate.format(now);
         DayOfWeek dayOfWeek = LocalDateTime.now().getDayOfWeek();
         String state = (dayOfWeek.equals(DayOfWeek.SATURDAY))|| (dayOfWeek.equals(DayOfWeek.SATURDAY))
                 ?"zamknięte"
                 : "otwarte";
         Instant date = Instant.now();
         model.addAttribute("state", state);
         model.addAttribute("date", date);

         return "contact";
     }
}
