package com.sda.dropbox.upload;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.mail.Email;
import com.sda.dropbox.mail.MailSender;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.sda.dropbox.config.Keys.*;

public class UploadDropBox implements Uploader  {
    private ConfigService cfg;
    private MailSender mailSender;

    public UploadDropBox(ConfigService cfg, MailSender mailSender) {
        this.cfg = cfg;
        this.mailSender=mailSender;
    }

    @Override
    public void upload(String path, String name) {
        // Create Dropbox client
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, cfg.get(DROPBOX_KEY));

        // Upload "test.txt" to Dropbox
        try (InputStream in = new FileInputStream(path)) {
            FileMetadata metadata = client.files().uploadBuilder("/"+name)
                    .uploadAndFinish(in);
            mailSender.seandMail(new Email(cfg.get(EMAIL_SUBJECT),cfg.get(EMAIL_CONTENT), cfg.get(EMAIL_TO)));
        } catch (UploadErrorException e) {
            e.printStackTrace();
        } catch (DbxException | IOException e) {
            throw new UploadException("Can not upload file" + path,e);
        }
    }
    }

