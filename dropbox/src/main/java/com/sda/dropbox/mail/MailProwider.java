package com.sda.dropbox.mail;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.mail.client.MailGrid;
import com.sda.dropbox.mail.client.MailJet;



import static com.sda.dropbox.config.Keys.MAIL_CLIENT;

public class MailProwider {

    private final ConfigService cfg;

    public MailProwider(ConfigService cfg){
        this.cfg=cfg;
    }

    public static MailSender get(ConfigService cfg){
        String client = cfg.get(MAIL_CLIENT);
        if(client.equals("mailJet")){
            return new MailJet();
        }else if(client.equals("mailGrid")){
            return new MailGrid(cfg);
        }else throw  new RuntimeException("Client not anavable");
    }

}
