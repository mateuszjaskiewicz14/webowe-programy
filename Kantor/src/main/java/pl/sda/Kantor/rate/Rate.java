package pl.sda.Kantor.rate;


public class Rate {
    private String name;
    private double rate;
    private double margin;

    public Rate(String name, Double rate, Double margin) {
        this.name = name;
        this.rate = rate;
        this.margin = margin;
    }

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    public double getSellRate(){return rate + (margin*rate);}
    public double getBuyRate(){return rate - (margin*rate);}


}
