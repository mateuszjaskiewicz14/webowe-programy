package pl.sda.Kantor.user;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "User")
public class User {
    @Id
    public String username;
    public String password;
    public Boolean enabled;
}
