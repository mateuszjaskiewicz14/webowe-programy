package pl.sda.Kantor.order;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.Kantor.rate.ExchangeRatesService;
import pl.sda.Kantor.rate.Rate;
import pl.sda.Kantor.wallet.WalletService;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final ExchangeRatesService exchangeRatesService;
    private WalletService walletService;

    public OrderService(OrderRepository orderRepository, ExchangeRatesService exchangeRatesService) {
        this.orderRepository = orderRepository;
        this.exchangeRatesService = exchangeRatesService;
    }

    @Transactional
    public void submitOrder (String currency, String currency1, Double value){
    Rate rate = exchangeRatesService.getRate(currency,currency1);
    }
}
