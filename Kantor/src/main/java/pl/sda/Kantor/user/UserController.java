package pl.sda.Kantor.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/user/register")
    public String userRegistratinon(){return "userRegistration";}

    @PostMapping("/user/register")
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("password2") String password2,
                           HttpServletRequest httpServletRequest,
                           Model model) throws ServletException{

        if(!password.equals(password2)){
            model.addAttribute("notMachPassword","Hasła nie są takie same");
            return "userRegistration";
        }
        try{
            String encodePassword = passwordEncoder.encode(password);
            jdbcTemplate.update("INSERT INTO uesr (username, password, enabled) VALUE (?,?,?)",
                    username, encodePassword, 1);
            jdbcTemplate.update("INSERT INTO authorities (username, authority) VALUE (?,?,?)",
                    username, "ROOL_USER");
        }catch(Exception e){
            model.addAttribute("error","Ten urzytkownik juz istnieje");
            return "usserRegistration";
        }
        httpServletRequest.login(username,password);

        return "redirection/contact";
    }
}
