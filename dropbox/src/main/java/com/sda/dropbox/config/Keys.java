package com.sda.dropbox.config;

public interface Keys {
    String DROPBOX_KEY = "key";
    String DIRECTORY = "dir";
    String MAIL_CLIENT = "mail.client";
    String EMAIL_TO = "email.to";
    String EMAIL_SUBJECT = "email.subject";
    String EMAIL_CONTENT = "email.content";
    String MAIL_KEY_GREAD = "keyMailGread";
}
