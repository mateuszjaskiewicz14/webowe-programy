package pl.sda.Kantor.user;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "autchorites")
public class UserAuthority {

    @Id
    public String username;
    public String authority;
}
