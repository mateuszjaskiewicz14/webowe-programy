package com.sda.dropbox.listener;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.upload.UploadException;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.sda.dropbox.config.Keys.DIRECTORY;

public class DirectoryListener {
    private final String dir;
    private Uploader uploadDropBox;
    private final ExecutorService executorService = Executors.newCachedThreadPool();


    public DirectoryListener(Uploader uploadDropBox, ConfigService cfg) {
        this.uploadDropBox = uploadDropBox;
        this.dir = cfg.get(DIRECTORY);
    }
    // Statystyki w pakiecie statas wysłane pliki ilość,
    // ilość plików na sekunde i ilość błędów i na sekunde tyż
    // odporna na wielo wontkowość. Atomiki w statystykach


    public void listen() {
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(dir);
            path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
            WatchKey key;

            while ((key = watchService.take()) != null) {


                String name = key.pollEvents().get(0).context().toString();
                System.out.println(name);
                executorService.submit(()-> uploadDropBox.upload(dir + name, name));
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            throw new UploadException("Path failor", e);
        }
    }
}
