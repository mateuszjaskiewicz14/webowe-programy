package pl.sda.Kantor.rate;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyCheckRepository extends CrudRepository<CurrencyChec,Integer> {
    long countByCurrency(String currency);

    @Query("SELECT c.currency FROM CurrencyChec c GROUP BY c.currency ORDER BY COUNT (c.currency) DESC")
    List<String> getMostPopularyCurrency();


}
