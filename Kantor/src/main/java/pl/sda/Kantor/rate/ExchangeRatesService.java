package pl.sda.Kantor.rate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class ExchangeRatesService {
    private static final String URL= "https://api.exchangeratesapi.io/latest?base=";

    private static class ExchangeRatesDto{
        public Map<String,Double> rates;

    }
    private RestTemplate restTemplate;
    private double margin;
    private Random random = new Random();

    @Autowired
    public ExchangeRatesService(RestTemplate restTemplate, @Value("2")double margin){
        this.restTemplate=restTemplate;
        this.margin = margin;
    }

    public List<Rate> getRates() {
        ExchangeRatesDto exchangeRates = restTemplate.getForObject(URL, ExchangeRatesDto.class);

        List<Rate> rateArrayList = new ArrayList<>();
        for(Map.Entry<String,Double> entry : exchangeRates.rates.entrySet()){
            String currency = entry.getKey();
            Double exchangRate = entry.getValue();

            double rand = random.nextInt(100);
            Double randomRate = (1+(rand/10000)*exchangRate);
            rateArrayList.add(new Rate(currency,exchangRate,margin));
        }


        return rateArrayList;
    }

    public Double getRate(String currency,String currency1) {
        ExchangeRatesDto exchangeRatesDto = restTemplate.getForObject(URL + currency, ExchangeRatesDto.class) ;
          return exchangeRatesDto.rates.get(currency1);
    }

    }





















