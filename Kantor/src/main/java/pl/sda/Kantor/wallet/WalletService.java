package pl.sda.Kantor.wallet;

import org.springframework.stereotype.Service;

@Service
public class WalletService {

    private final WalletRepository walletRepository;

    public WalletService(WalletRepository walletRepository ) {this.walletRepository = walletRepository;}

    public synchronized void add(String currency, Double value){
        WalletEntry walletEntry = new WalletEntry();
        walletEntry.setCurrency(currency);
        walletEntry.setChange(value);
        walletRepository.save(walletEntry);
    }

    public synchronized void substract(String currency, Double value){
        Double walletAmount = walletRepository.getWalletAmount(currency);
        if(walletAmount == null || value>walletAmount){
            throw new RuntimeException("Not enough to substract " + value + " " + currency);
        }
        WalletEntry walletEntry = new WalletEntry();
        walletEntry.setCurrency(currency);
        walletEntry.setChange(-value);
        walletRepository.save(walletEntry);
    }
}
