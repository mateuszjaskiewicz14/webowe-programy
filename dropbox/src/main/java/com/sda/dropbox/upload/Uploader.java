package com.sda.dropbox.upload;

public interface Uploader {
    public void upload(String path, String name);
}
