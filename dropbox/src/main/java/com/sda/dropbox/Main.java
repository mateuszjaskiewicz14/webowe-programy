package com.sda.dropbox;


import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.listener.DirectoryListener;
import com.sda.dropbox.mail.MailProwider;
import com.sda.dropbox.mail.MailSender;
import com.sda.dropbox.upload.UploadDropBox;


import java.io.IOException;

public class Main {
    private static final int PROPS_INDEX = 0;

    public static void main(String[] args) throws IOException, InterruptedException {
        String propertiesPath = args[PROPS_INDEX];
        ConfigService cfg = new ConfigService(propertiesPath).load();
        MailSender mailProwider = MailProwider.get(cfg);
        UploadDropBox uploadDropBox = new UploadDropBox(cfg,mailProwider );
        DirectoryListener listener = new DirectoryListener(uploadDropBox, cfg);
        listener.listen();
    }

}
