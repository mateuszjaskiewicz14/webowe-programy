package pl.sda.Kantor.rate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RateControler {
    private ExchangeRatesService exchangeRatesService;
    public RateControler(ExchangeRatesService exchangeRatesService ){
        this.exchangeRatesService = exchangeRatesService;
    }
//    private List<Rate> rates = Arrays.asList(
//
//    );
    @GetMapping("/rates")
    public String getRates(Model model){
        model.addAttribute("rates", exchangeRatesService.getRates());
        return "rates";
    }

    @GetMapping("/calculator")
    public String calculator(Model model){
        model.addAttribute("rates", exchangeRatesService.getRates());
        return "calculator";
    }

    @PostMapping("/calculator")
    public  String calculatonResoult(
            Model model,
            @RequestParam("currency")String curenrency,
            @RequestParam("currency1")String curenrency1,
            @RequestParam("value")Double value){
        Double rate = exchangeRatesService.getRate(curenrency,curenrency1);
        Double calculatorResoult =value * rate;
        model.addAttribute("value",value);
        model.addAttribute("curenrency",curenrency);
        model.addAttribute("curenrency1",curenrency1);
        model.addAttribute("calculatorEstymate",calculatorResoult);
        return "calculationResoult";
    }



}
