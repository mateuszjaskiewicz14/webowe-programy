package pl.sda.Kantor.admin;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.Kantor.order.CurrencyOrder;
import pl.sda.Kantor.order.OrderRepository;
import pl.sda.Kantor.rate.CurrencyCheckRepository;

@Controller
@RequestMapping("/admin")
public class AdminControl {
    private CurrencyCheckRepository currencyCheckRepository;
    private OrderRepository orderRepository;

    public AdminControl(CurrencyCheckRepository currencyCheckRepository){
        this.currencyCheckRepository = currencyCheckRepository;
    }

    @GetMapping("/conversion/stats")
    public String getStats(Model model){
        model.addAttribute("allCheckCount",currencyCheckRepository.count());
        model.addAttribute("eurChecksCount",currencyCheckRepository.countByCurrency("EUR;"));
        model.addAttribute("mostPopular", currencyCheckRepository.getMostPopularyCurrency());
        return"adminStats";
    }

    @GetMapping("/orderlist")
    public String getOrders(Model model){
        Iterable<CurrencyOrder> orders = orderRepository.fingAll();
        model.addAttribute("orders", orders);
        return"orders";
    }
}
